<!--- <cflocation> wrapper --->
<cffunction name="relocate" description="this is a &lt;cflocation&gt; wrapper.">
	<cfargument name="url" required="yes">
	<cflocation url="#arguments.url#">
</cffunction>
<cfscript>
	request.relocate = relocate;
</cfscript>