<cffunction name="include">
	<!--- arg(s) --->
	<cfargument name="tpl" type="string">
	<!--- body --->
	<cfif FileExists(ExpandPath(tpl))>
		<cfinclude template=#tpl#>
		<cfreturn true>
	<cfelse>
	  <cfreturn false>
	</cfif>
</cffunction>

<cffunction name="include_all">
	<!--- arg(s) --->
	<cfargument name="dir" type="string">
	<cfargument name="filter" type="string">
	<!--- body --->
	<cfset var expDir = ExpandPath(dir)>
	<cfif DirectoryExists(expDir)>
		<cfdirectory directory=#expDir# name="files" type="file" filter=#filter# listinfo="name">
		<cfloop query="files">
			<cfset tpl = '#dir#/#files.name#'><!---<cfdump var=#tpl#>--->
			<cfif FileExists(ExpandPath(tpl))>
				<cfinclude template=#tpl#>
			</cfif>
		</cfloop>
		<cfreturn true>
	<cfelse>
		<cfreturn false>
	</cfif>
</cffunction>