<!--- <cfhttp> wrapper --->
<cffunction name="httpi" description="this is a &lt;cfhttp&gt; wrapper that accepts a struct-formatted postbody.">
	<!--- arg(s) --->
	<cfargument name="method" default="get">
	<cfargument name="url" required="yes">
	<cfargument name="postbody">
	<cfargument name="username">
	<cfargument name="password">
	<!--- local scope --->
	<cfset var paramtype = "formfield">
	<!--- body --->
	<cfhttp method="#arguments.method#" url="#arguments.url#" timeout="5" useragent="application.applicationname">
		<!--- if username&&password, use authentication --->
		<cfif isdefined("arguments.username") and isdefined("arguments.password")>
			<cfhttpparam type="formfield" name="username" value=#arguments.username#>
			<cfhttpparam type="formfield" name="password" value=#arguments.password#>
		</cfif>
		<!--- if method=post && postbody, parameterize postbody --->
		<cfif isdefined("arguments.postbody")>
			<cfloop collection=#arguments.postbody# item="param">
				<cftry><!--- 'formfield' is the default/implicit type --->
					<cfset paramtype = #arguments.postbody[param].type#>
					<cfcatch></cfcatch>
				</cftry>
				<!---
				<cfif not isdefined("arguments.postbody[param].type")>
					<cfset arguments.postbody[param].type = 'formfield'>
				</cfif>
				--->
				<cfhttpparam
					type="#paramtype#"
					name="#arguments.postbody[param].name#"
					value="#arguments.postbody[param].value#"
				>
			</cfloop>
		</cfif>
	</cfhttp>
	<!--- we could do additional response processing; error-handling, proxy-handling, write/overwrite response headers, caching, etc 
	<cfif '#cfhttp.statuscode#' eq '200 OK'>
		<cfheader name = "Last-Modified" value = "">
		<cfheader name = "Cache-Control" value = "max-age=8640000">
		<cfheader name = "Expires" value = "Sat, 26 DEC 2009 16:00:00 GMT">
		<cfcontent type='#cfhttp.mimetype#'>
		<cfoutput>
			#cfhttp.filecontent#
		</cfoutput>
	<cfelse>
		<cfoutput>
			http status: #cfhttp.statuscode#<br />error details: #cfhttp.errordetail#
		</cfoutput>
	</cfif>--->
	<cfreturn cfhttp>
</cffunction>