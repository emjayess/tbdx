<!---
	IMPORTANT @TODO:
		implement cfqueryparam: http://livedocs.adobe.com/coldfusion/8/htmldocs/help.html?content=queryDB_5.html#1142588
		loop over query parameters such as is done with httpi() parameter structures!
--->
<cffunction name="query" access="public" returntype="query">
  <cfargument name="sql" type="string" required="yes">
  <cfargument name="dsn" type="string" required="no" default="#application.database#">
  <cfargument name="dbType" type="string" default="">
	  <cfquery name="rs" datasource="#arguments.dsn#" dbtype="#arguments.dbType#">
		  #preserveSingleQuotes(arguments.sql)#
	  </cfquery>
	  <cfreturn rs>
</cffunction>

<cfscript>
	request.query = query;

	request.queries = {};

	function dbproxy(sql,dsn) {
		var rs = {result = request.query(sql,dsn)};
		structappend(request.queries, rs);
		return rs.result;
	}
	request.dbproxy = dbproxy;

	function get(sql) {
		return request.dbproxy(sql,request.dsn_read);
	}
	request.get = get;

	function put(sql) {
		return dbproxy(sql,request.dsn_write);
	}
	request.put = put;
</cfscript>