<!--- <cfcookie> wrapper --->
<cffunction name="bake" description="&lt;cfcookie&gt; wrapper; cookie setting operations">
	<!---arg(s)--->
	<cfargument name="name">
	<cfargument name="value">
	<cfargument name="expiry" required="no">
	<!---body--->
	<cfif isdefined("arguments.expiry")>
		<cfcookie
			name="#arguments.name#"
			value="#arguments.value#"
			domain=".#cgi.http_host#"
			expires="#arguments.expiry#"
		>
	<cfelse>
		<cfcookie
			name="#arguments.name#"
			value="#arguments.value#"
			domain=".#cgi.http_host#"
		><!---expiry implicitly defaults to "session", e.g. browser close--->
	</cfif>
</cffunction>

<!---
	copy [variables.]bake() to REQUEST scope
	(
		this is probably only necessary if custom tags are involved/invoked and require access to the function
		SEE: "Specifying the scope of a function" http://livedocs.adobe.com/coldfusion/8/htmldocs/help.html?content=UDFs_16.html#1117954
	)
--->
<cfset request.bake=variables.bake>