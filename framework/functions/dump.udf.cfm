<!--- <cfdump> wrapper --->
<cffunction name="dump" returntype="boolean" output="yes"
	description="a &lt;cfdump&gt; wrapper function for use via &lt;cfscript&gt; blocks."
	displayname="dump();"
	hint="dump(myvariable);">
	<!--- arg(s) --->
	<cfargument name="var" type="any" default="nothing to dump">
	<cfargument name="lbl" required="no">
	<!--- body --->
	<cfdump var="#arguments.var#">
	<cfreturn true>
</cffunction>