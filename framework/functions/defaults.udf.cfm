<cfscript>
	function isset(variable) {
		//isvalid();?
		return defaults(dontknow,true);//uh-oh recursion
	}
	/**
	 * if isdefined and isset, return variable
	 * else set variable to value & return value
	 *
	 * NOTE: this mimics <cfparam> functionality,
	 * (which has a couple entries @ cflib.org)
	 */
	function defaults(variable,value) {
		// return variable, if isdefined and isset
		// else set and return 'value'
		if (isdefined(variable) and isset(variable)) {
			return variable;
		}
		else {
			return value;
		}
	}

	// sample usage:
	// var myVar = param(value=arguments[N],defaultValue='myDefault');
	function param(param) {
		
	}
</cfscript>