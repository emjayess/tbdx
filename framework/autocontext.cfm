<cfscript>
/*
a 'request.context' singleton object...

some of the fields
 * page
 * user/viewer (history/click-path/recently-viewed)
 * user-agent
*/
function autocontext(){
	if (not isdefined('page')) page = 'home';
}
autocontext();
</cfscript>