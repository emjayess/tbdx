<cfscript>

	// is it silly to scope request data into application scope? yes.
	// don't do this, there is already an appropriate 'request' scope for this.
	//application.http_request = gethttprequestdata();


	// route registry
	//
	// partially inspired by the rails 3 routes.rb approach
	// (see also cfwheels: http://cfwheels.org/docs/1-1/chapter/using-routes)
	// e.g.
	//		match 'posts/:id', :to => 'controller#action'
	// e.g.
	//		[controller] / [action] / [key]
	// or
	//		[controller] / [key]
	// with an implicit/default 'action', like 'index'
	//
	// a url like: http://mysite.dev/posts/1 would map to the index() action of the posts controller

	function autoroutes() {
		var routes = {
			root = {//root controller
				route = '/',
				handler = 'index'
			},
			404 = {//error controller
				route = '/oops',
				handler = 'error.404'
			},
			// redirect controller??

			1 = {
				route = '/search',
				route_aliases = '/find/[name],/find/[keyword],/find[wildcard]',
				handler = 'tbdx.search'// invokes /framework/cfc/tbdx.cfc search() method
			},
			2 = {
				route = '/list/[id]',
				route_aliases = '/list/[name],/list/[custom]',
				handler = 'tbdx.list'
			}
		};
		return routes;
	}

	application.routes = autoroutes();

	// de-reference url request path
	urlwhack = '/';
	path = iif (isdefined("url.q"),
		evaluate(de('url.q')),
		de(DEFAULT_CONTEXT)
	);

	context = listfirst(
		path,
		urlwhack
	);

	args = listtoarray(
		listrest(
			path,
			urlwhack
		),
		urlwhack
	); //dump(args[1]);

	if (isajax(application.http_request)) {//content negotiation
		try { include('/' & context & '.cfm'); }
		catch (any e) { /*watchtower(e);*/ }
	}
	else {// just include main controller template
		include('/themes/tbdx/index.mustache');
		dump(application.routes);
	}
</cfscript>