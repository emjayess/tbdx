<cfinclude template="/framework/udf/include.helper.cfm">
<cfscript>
	include_all('/framework/udf','*.udf.cfm');

	$ = createobject('component', 'cfc.$');
	bouncer = createobject('component', 'cfc.authenticator');
	auditor = createobject('component', 'cfc.auditor');
	mustache = createobject('component','cfc.mustache');

	include('/framework/autocontext.cfm');

	//TODO: routing system
	//include('/framework/autorouter.cfm');
	/**
	 * consider scoping framework UDF's to request or application scopes
	 *  application scope may require too much messing with <cflock>??
	 *   http://livedocs.adobe.com/coldfusion/8/htmldocs/help.html?content=UDFs_16.html#1117954
	 */


//possibly useful someday
/*
	if(IsDefined("MyFunc")) 
		if(IsCustomFunction(MyFunc)) 
			WriteOutput("MyFunc is a user-defined function"); 
		else 
			WriteOutput("Myfunc is defined but is NOT a user-defined function"); 
		else 
			WriteOutput("MyFunc is not defined"); 
			*/

</cfscript>