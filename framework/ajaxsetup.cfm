<script>
// ajaxsetup.js
(function($){
	$.ajaxSetup({
		beforeSend:function(xhr){
			function safeSet(k,v){
				try{ xhr.setRequestHeader(k,v); }
				catch(e){/*throw? */}
			}
			// request header hacks
			// see: http://blog.mibbit.com/?p=143
			safeSet('User-Agent','blah blah ');
			safeSet('X-Requested-With','tbdx');
			safeSet('Totally-Custom-Header','Totally Radical Value');
		}
	});

	// Learning jQuery pg. 129
	$().ajaxStart(function() { $('#busy').slideDown(); });
	$().ajaxStop(function() { $('#busy').slideUp('slow'); });
})(jQuery);
</script>