<cfscript>
	request.databases = {
		master = '',
		slave_1 = '',
		slave_2 = '',
		slave_N = ''
	};

	db_statuses = '';
	cache_validity_key = 'a';
  
	//createObject('java','java.lang.Class').forName('sun.jdbc.odbc.JdbcOdbcDriver');
	//createObject('java','java.lang.Class').forName('com.mysql.jdbc.Driver');
	//function QueryTable(){ return createObject('java','coldfusion.sql.QueryTable'); }
	QueryTable = createObject('java','coldfusion.sql.QueryTable');
  
	function query(sql,server){
		isdev = iif(/*dev-environment-expression-here*/,true,false);
		db = iif(isdefined(arguments[3]), 
			evaluate(de(arguments[3])),
			de('default_database')
		);
		username = iif(isdev,
			de('dev_username'),
			de('live_username')
		);
		password = iif(isdev,
			de('dev_password'),
			de('live_password')
		);
		try{
			return QueryTable.init(
				createObject('java','java.sql.DriverManager').getConnection(
					'jdbc:odbc:DRIVER={SQL Server};database='&db&';server='&server&';',username,password
				).createStatement().ExecuteQuery(sql)
			);
		}
		catch(any e){
			watchtower(e); 
		}
	}
	
	/**
	 * $cache()
	 *  an adhoc memory caching proxy in front of database queries
	 */
	function $cache(sql,server){
		var response = {
			viacache = true,
			ckey = cache_validity_key & ':SQL:' & server & ':' & md5(sql),
			data = cache.get(response.ckey)
		};

		if(isSimpleValue(response.data) and response.data==""){//check empty string
			response.viacache = false;
			response.data = query(sql,server);
			response.set = cache.set(
				response.ckey,
				response.data,
				"3600"
			);
		}
		return response;
	}
	
	/**
	 * queryMySQL()
	 */
	function queryMySQL(sql){
		try{
			return QueryTable.init(
				createObject('java',
					'java.sql.DriverManager'
				)
				.getConnection(
					'jdbc:mysql://db.domain.tld/database',
					'username',
					'password'
				)
				.createStatement()
				.ExecuteQuery(sql)
			);
		}
		catch(any e){
			watchtower(e);
		}
	}
</cfscript> 
