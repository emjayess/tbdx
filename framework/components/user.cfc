<cfcomponent displayname="user">
<cfscript>

	function init() {
		variables.context = {};
		return this;
	}

	/**
	 * @fn: user.login(u,p) / auth.login(u,p,secret)
	 */
	function _login(username,password) {
		var rs = get(
			//queries user model or data…
		);
		if (rs.recordcount) {
			this.id = rs.id
			//todo: populate an entire user object / profile here!
			return this.id;
		}
		else {
			return false;
		}
	}

	/**
	 * @fn: user.logout() / auth.logout()
	 */
	function logout(args) {
		//@TODO
	}

	function prep_localstorage() {
		// the following data will be serialized into the browser's local storage, and/or kept in cookies
		/*
		cookie.user_id = {//idea
			user_id = user.id,
			token = user.token
		}*/
	}
	
	function _logout(){
		//kill session, unset cookie
		bake("user_id",user.id,"now");
	}

	function masquerade_as(user){//fake being logged in as an alternate account…
		cookie.user_id = #user.id#;
		userinfo.type = #user.role#;
	}
	
</cfscript>
</cfcomponent>