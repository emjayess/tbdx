<!---
	use this from a url (or apache mod-rewrite controller proxy) like:
	http://mysite/controller.cfc?method=route&route=/path/to/content
--->
<cfcomponent name="controller" access="remote">
	<cffunction name="route">
		<cfargument name="route" type="string">
	</cffunction>
</cfcomponent