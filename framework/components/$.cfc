<cfcomponent displayname="bling">
<cfscript>

	function init() {
		variables.context = {};
		return this;
	}

	/**
	 * $.iterator(selection)
	 * hint: myIterator = $.iterator(['one','two','three']);
	 */
	function iterator(selection) {
		return selection.Iterator();
		// todo: modify this to enable selection.each(callback) syntax
	}

	/**
	 * $.getJSON(uri)
	 * used to fetch & parse JSON fixtures; designed to resemble jQuery's syntax + functionality
	 */
	function getJSON(fixture) {
		var json = fileread(fixture);
		return deserializejson(json);
	}

	function getTMPL(mustache) {
		return fileread(mustache);
	}

	function merge(a,b) {
		//@todo: accept an arbitrary number of arguments, and recursively merge them all
		// see also: structupdate(), structcopy(), structinsert(), structclear(), and structdelete()
		// note that objects & structs (but not arrays) are passed by ref, so returning it is superfluous(!)
		if (not structappend(a,b)) {
			// report/log merge problem
		}
	}

	/**
	 * $.render(data,tmpl)
	 * short-hand access to mustache.render
	 */
	function render(ngin,view,data) {
		writeoutput(
			ngin.render(view,data)
		);
	}

	/**
	 * freeze
	 */
	function freeze(assets) {
		//see: github.com/openfcci/frozenassets
	}
</cfscript>
</cfcomponent>