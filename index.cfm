<cfscript>
// move into global controller
bouncer.check_whogoesthere();
bouncer.userrole();

//these could be put into request scope? would seem sensible.
index_html = $.getTMPL(template_path & "index.mustache");
index_data = $.getJSON(fixtures_path & "index.json");

// controller logic, then append more 'index_data', er 'view_data', etc
$.merge(
	index_data,
	$.getJSON(fixtures_path & "navigation.json")
);

$.merge(
	index_data,
	bouncer.username()
);

// devel + debugger stuff
//dump(request.queries);

writeoutput(
	mustache.render(
		index_html,
		index_data
	)
);
</cfscript>
<!---//none of these whitespace 'tricks' had any effect
<cfsetting enablecfoutputonly="true"></cfsetting>
<cfprocessingdirective sauppresswhitespace="true">
	<cfoutput>#trim(html)#</cfoutput>
</cfprocessingdirective>--->